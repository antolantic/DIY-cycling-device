# DIY Cycling Device


![DIY Trainer](doc/img/20200417_213758.jpg)


Construire avec des matériaux de récupération un home trainer connecté à Zwift 
Technologies : GATT BLE, Arduino, Raspberry.


![General Diagram](doc/graphics/General_Diagram.png)

# De quoi avez vous besoin ?

- Arduino
- Raspberry Pi avec BLE  (RPi 3 dans mon cas)
- Capteur effet Hall
- En option : transistor NPN si vou avez récupéré le capteur dans un ventilo de PC
- Aimant Néodyme 

# Construction

Cf [Wiki](https://gitlab.com/antolantic/DIY-cycling-device/-/wikis/Pr%C3%A9sentation)


# Comment lancer le programme

Se connecter au raspberry en ssh et exécuter le script python 2.x (non compatible python 3 pour le moment) cycling.py script :

```
python cycling.py
```