# Import de la librairie serial
import serial

# Ouverture du port serie avec :
# '/dev/ttyXXXX' : definition du port d ecoute (remplacer 'X' par le bon nom)
# 9600 : vitesse de communication
serialArduino = serial.Serial('/dev/ttyACM0', 115200)


# Ecriture de chaque message recu
while True :
    received = serialArduino.readline()
    print(received)
    data = received[1:-3].split(',')
    print(data)
    print(data[0])
    print(data[1])
    print("Last wheel event time {} : ".format(data[0]))
    print("Cumulative wheel revolution : {} ".format(data[1]))