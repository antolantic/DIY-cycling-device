// digital pin 2 is the hall pin
int hall_pin = 2;
int led_pin = 13;
int redpin = 11; // select the pin for the red LED
int greenpin = 10 ;// select the pin for the green LED
int bluepin = 9; // select the pin for the blue LED

int val;
  
// set number of hall trips for RPM reading (higher improves accuracy)
float hall_thresh = 100.0;
const float circum = 210.0;

volatile unsigned long detect_time = 0;
volatile unsigned long last_detect_time = 0;
volatile unsigned long duration = 0;
volatile unsigned int velocity = 0;

void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);

 // RGB led
  
  pinMode (redpin, OUTPUT);
  pinMode (bluepin, OUTPUT);
  pinMode (greenpin, OUTPUT);

  
  // make the hall pin an input:
  Serial.print("Init ");
  pinMode(hall_pin, INPUT_PULLUP);
  //pinMode(led_pin, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(hall_pin), detect_up, RISING);
}

// the loop routine runs over and over again forever:
void loop() {
//  Serial.println(duration);
//  delay(1);        // delay in between reads for stability
 // If duration > 3s then the wheel is stopped -> speed = 0s
  if ( (millis() - last_detect_time) > 3000) velocity=0;
  Serial.print("VITESSE : ");
  Serial.println(velocity);
  if (velocity <= 10 ) setColor(255, 255, 255);
  else if (velocity > 10 && velocity <= 20) setColor(0, 255, 0);
  else if (velocity > 20 && velocity <= 30) setColor(250, 105, 0);
  else if (velocity > 30 && velocity <= 40) setColor(250, 40, 0);
  else if (velocity > 40) setColor(255, 0, 0);
}


void detect_up() {
//  Serial.println("DETECT UP");
  duration = millis() - detect_time;
  detect_time = millis();
//  Serial.print("Duration : ");
//  Serial.println(duration);
  velocity = calc_velocity_kmh(duration);
  Serial.print("Velocity : ");
  Serial.print(velocity);
  Serial.println(" km/h");
  // This is needed
  
  last_detect_time = detect_time;
  digitalWrite(led_pin, HIGH);
}

int calc_velocity_kmh(long duration) {

  float distance_km = (float)circum / 100000;
  float duration_h = (float)duration / (3600000);  
  if (duration_h == 0) return 0;
 
  else return distance_km / duration_h;
}

void setColor(int red, int green, int blue)
{
    analogWrite(redpin, red);
    analogWrite(greenpin, green);
    analogWrite(bluepin, blue);
    delay(10);
}

//    setColor(250, 105, 0);   // Yellow
//    delay(1000);
//
//    setColor(250, 40, 0);    // Orange
//    delay(1000);
//
//    setColor(255, 0, 0);     // Red
//    delay(1000);
//
//    setColor(10, 10, 255);   // Blue
//    delay(1000);
//
//    setColor(255, 0, 100);   // Pink
//    delay(1000);
//
//    setColor(200, 0, 255);   // Purple
//    delay(1000);
//
//    setColor(0, 255, 0);     // Green
//    delay(1000);
//
//    setColor(255, 255, 255); // White
//    delay(1000);
